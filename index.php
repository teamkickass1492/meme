<?php 
session_start();
//setcookie(name, value, expire, path, domain, secure, httponly);
$memecookie = $memeimage;
$textcolor = $fontcolor;
setcookie($memecookie, time() + (86400));
?>

<!DOCTYPE HTML>
<html lang="en">
    <meta charset="utf-8">
    <title> Assignment 3 | Meme Generator</title>
    <link rel="stylesheet" href="main.css">
    <script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
        <script src="jqmemeload.js"></script>
    
    <!-- JS file for the dynamic color picker -->
    <script src="colorjs.js"></script>
    
    <body>
	<!-- PHP variables for default values -->
	<?php $memeimage = $_GET['memepicker']; 
	      $fontcolor = $_GET['colorpicker']; ?>
	
	<img id="defaultimage" src="<?php if ($memeimage == '') {echo 'memeuploads/fry.jpg';} else {echo $memeimage;}?>">

	<!-- Main page form -->
	<form action="<?php echo $PHP_SELF;?>" method = "GET">

	<!-- Textbox Upper -->
        <div id="textinput1">Top: <input type="text" name="toptext" class ="topt" id = "topt" value="<?php if (isset($_GET['toptext'])) echo $_GET['toptext']; ?>"></div>
        <script src="toptext.js"></script>

	<!-- Textbox Lower -->
        <div id="textinput2">Bottom: <input type="text" name="bottomtext" class="bottomt" id = "bottomt" size="30" value="<?php if (isset($_GET['bottomtext'])) echo $_GET['bottomtext']; ?>" style="display:inline-block;font-size:14px"></div>
        <script src="bottext.js"></script>
	       
	<br>
	
	<!-- This is the php driver for generating the Meme selection menu -->
	<?php include 'test_json.php';?>   
	
    <!-- The pic List JS file helps in making the image selection dynamic -->
    <!--    <script src="pic_list.js"></script> 
	<script type="text/javascript">
  		document.getElementById('memepicker').value = "<?php //echo $_GET['memepicker'];?>";
	</script>-->
	
	<!-- HTML5 color Picker -->
	<div>Choose Font Color: <input id="colorpicker" name="colorpicker" onchange="changecolor(this.colorpicker)" value="<?php if (isset($_GET['colorpicker'])) echo $_GET['colorpicker']; ?>" style="width:25px;" type="color"/>

	<!-- Submit the form -->
	<br>
        <input type="submit" name="Submit" value="Submit">
	
	</form>
	
	<!-- TOP MEME TEXT -->
        <div id="memetext1" style="color:<?php if($fontcolor == '') {echo 'white';} else {echo $fontcolor;}  ?>;"><?php echo $_GET['toptext']; ?></div>
	
        <!-- BOTTOM MEME TEXT -->
	<div id="memetext2" style="color:<?php if($fontcolor == '') {echo 'white';} else {echo $fontcolor;} ?>;"><?php echo $_GET['bottomtext']; ?></div>
        
	<br><br>
    	<a href="login.php">Admin Page</a>

	</body>
</html>
